package de.franden.rsredirect;

import org.glassfish.jersey.apache.connector.ApacheConnectorProvider;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;

public class JerseyClientTest {

    private static final String URL_TO_TEST = "http://twitter.com";

    /**
     * works like described in resteasy documentation
     */
    @Test
    public void testRedirectWorks() {

        Response response =
                createApacheHttpClient().target(URL_TO_TEST).request().get();

        printDetails(response);
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
    }

    /**
     * Redirect from HTTP to HTTPS will not work. see
     * https://bugs.java.com/bugdatabase/view_bug.do?bug_id=4620571
     * https://stackoverflow.com/questions/1884230/urlconnection-doesnt-follow-redirect
     * <p>
     * "After discussion among Java Networking engineers, it is felt that we shouldn't automatically follow redirect from one protocol to another,
     * for instance, from http to https and vise versa, doing so may have serious security consequences. Thus the fix is to return the server
     * responses for redirect. Check response code and Location header field value for redirect information. It's the application's responsibility
     * to follow the redirect."
     */
    @Test
    public void testRedirectDoesNotWork() {

        Response response =
                createDefaultClient().target(URL_TO_TEST).request().get();

        printDetails(response);
        assertEquals(Response.Status.Family.REDIRECTION, response.getStatusInfo().getFamily());
    }

    private void printDetails(Response response) {
        response.getHeaders().forEach((key, value) -> System.out.println(key + ": " + value));
        System.out.println("http code:" + response.getStatus());
    }


    private Client createDefaultClient() {
        return ClientBuilder.newClient().property(ClientProperties.FOLLOW_REDIRECTS, Boolean.TRUE);
    }

    private Client createApacheHttpClient() {
        ClientConfig cc = new ClientConfig().connectorProvider(new ApacheConnectorProvider());
        return ClientBuilder.newClient(cc);
    }

}
