package de.franden.rsredirect;

import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.engines.ApacheHttpClient43Engine;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;

public class ResteasyClientTest {

    private static final String URL_TO_TEST = "http://twitter.com";

    @Test
    public void testRedirectWorks() {

        Response response = createClientWithRedirection().target(URL_TO_TEST).request().get();

        printDetails(response);
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
    }

    @Test
    public void testRedirectDoesNotWork() {

        Response response = ClientBuilder.newClient().target(URL_TO_TEST).request().get();

        printDetails(response);
        assertEquals(Response.Status.Family.REDIRECTION, response.getStatusInfo().getFamily());
    }

    private void printDetails(Response response) {
        response.getHeaders().forEach((key, value) -> System.out.println(key + ": " + value));
        System.out.println("http code:" + response.getStatus());
    }

    /**
     * redirect are disabled by default. Check the Resteasy documentation for more information
     * https://docs.jboss.org/resteasy/docs/3.5.0.Final/userguide/html_single/#http_redirect
     *
     * @return
     */
    private Client createClientWithRedirection() {
        ApacheHttpClient43Engine engine = new ApacheHttpClient43Engine();
        engine.setFollowRedirects(true);
        return new ResteasyClientBuilder().httpEngine(engine).build();
    }
}
